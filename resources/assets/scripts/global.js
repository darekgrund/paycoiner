'use strict';
(function ($) {
    var $header = $('.js-header');
    $(document).ready(function () {
        // Mobile menu
        var $mobileNavContainer = $('.js-mobile-nav--container');
        $mobileNavContainer.each(function () {
            var $mobileNav = $(this).find('.js-mobile-nav--nav');
            var $mobileNavSwitch = $(this).find('.js-mobile-nav--switch');
            var toggleNavigation = function () {
                $mobileNavSwitch.toggleClass('open');
                $mobileNav.slideToggle();
            };

            $mobileNavContainer.on('click', '.js-mobile-nav--switch', toggleNavigation);

            $mobileNavContainer.on('click', '.nav__link', function() {
                if ($mobileNavSwitch.hasClass('open')) {
                    toggleNavigation();
                }
            });
        });
    });

    $(window).scroll(function () {
        var scrollTop = $(this).scrollTop();
        if (scrollTop > 150) {
            $header.addClass('header--slim');
        } else if (scrollTop < 100) {
            $header.removeClass('header--slim');
        }
    });
    $(window).trigger('scroll');
})(jQuery);
